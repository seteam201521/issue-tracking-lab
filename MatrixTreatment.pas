unit MatrixTreatment;

interface
const RowCount = 9;
      ColumnCount = 9;
      MinValue = -15;
      MaxValue = 13;

type TasksMatrix = array[1..RowCount,1..ColumnCount] of Integer;

procedure FillMatrixWithRandomValues(var matrix: TasksMatrix);
procedure ZeroElementsAboveSecondaryDiagonal(var inputMatrix:
implementation
procedure FillMatrixWithRandomValues(var matrix: TasksMatrix);
begin
   var i,j: Integer;
begin
    Randomize;
    for i := 1 to RowCount do
    begin
        for j := 1 to ColumnCount do
        begin
            matrix[i, j] := Random(MaxValue-MinValue+1) + MinValue;
        end
    end
end;

procedure ZeroElementsAboveSecondaryDiagonal(var inputMatrix: TasksMatrix);
begin
    var i,j: Integer;
begin
    for i := 1 to RowCount do
    begin
        for j := 1 to ColumnCount-i do
        begin
            if inputMatrix[i,j]>0 then
            begin
                inputMatrix[i,j] := 0;
            end;
        end;
    end;
end;

end.
  